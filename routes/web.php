<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('/admin')->group(function(){
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/createUser', 'AdminController@showCreateUserForm')->name('createUserForm');
    Route::post('/createUser', 'AdminController@createUser')->name('createUser');

    Route::get('/userList', 'AdminController@showUserList')->name('userList');

    Route::get('/updateUser/{id}', 'AdminController@showUpdateUserForm')->where('id', '[0-9]+')->name('updateUserForm');
    Route::put('/updateUser', 'AdminController@updateUser')->name('updateUser');

    Route::put('/updatePassword', 'AdminController@updatePassword')->name('updateUser');

    Route::get('/updateData', 'DataController@showUpdateDataForm')->name('updateDataForm');
    Route::post('/updateData', 'DataController@updateData')->name('updateData');

    Route::get('/', 'Auth\LoginController@showLoginForm');
    Route::post('/', 'Auth\LoginController@login')->name('login');
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
});
