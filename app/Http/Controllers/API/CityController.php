<?php

namespace App\Http\Controllers\API;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\City;
use App\CityHistory;

class CityController extends Controller
{
    public function get_cities_status()
    {
        $hists = DB::select("SELECT cities.id, name, code, infected, healed, diseased FROM cities "
            ."JOIN (SELECT city_histories.city_id, infected, healed, diseased FROM city_histories "
            ."JOIN (SELECT city_id, MAX(created_at) AS orderDate FROM city_histories GROUP BY city_id) AS orderHistories "
            ."ON city_histories.city_id = orderHistories.city_id AND city_histories.created_at = orderHistories.orderDate) AS histories "
            ."ON cities.id = histories.city_id ORDER BY cities.id");

        $container = null;

        foreach($hists as $hist)
        {
            $container[$hist->code]['id'] = $hist->id;
            $container[$hist->code]['name'] = $hist->name;
            $container[$hist->code]['code'] = $hist->code;
            $container[$hist->code]['infected'] = $hist->infected;
            $container[$hist->code]['healed'] = $hist->healed;
            $container[$hist->code]['diseased'] = $hist->diseased;
        }

        return response()->json(['cities' => $container], 401);
    }
}
