<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\UpdateDataRequest;
use App\CityHistory;
use App\User;
use Auth;

class DataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showUpdateDataForm()
    {
        $data = DB::select("SELECT cities.id, name, infected, healed, diseased FROM cities "
        ."JOIN (SELECT city_histories.city_id, infected, healed, diseased FROM city_histories "
        ."JOIN (SELECT city_id, MAX(created_at) AS orderDate FROM city_histories GROUP BY city_id) AS orderHistories "
        ."ON city_histories.city_id = orderHistories.city_id AND city_histories.created_at = orderHistories.orderDate) AS histories "
        ."ON cities.id = histories.city_id ORDER BY cities.id");

        return view('data.updateData', ['data' => $data, 'success' => false]);
    }

    public function updateData(UpdateDataRequest $request)
    {
        $validated = $request->validated();

        $hist = new CityHistory();
        $hist->city_id = $validated['id'];
        $hist->user()->associate(Auth::user());
        $hist->infected = $validated['infected'.$validated['id']];
        $hist->healed = $validated['healed'.$validated['id']];
        $hist->diseased = $validated['diseased'.$validated['id']];
        $hist->save();

        $data = DB::select("SELECT cities.id, name, infected, healed, diseased FROM cities "
        ."JOIN (SELECT city_histories.city_id, infected, healed, diseased FROM city_histories "
        ."JOIN (SELECT city_id, MAX(created_at) AS orderDate FROM city_histories GROUP BY city_id) AS orderHistories "
        ."ON city_histories.city_id = orderHistories.city_id AND city_histories.created_at = orderHistories.orderDate) AS histories "
        ."ON cities.id = histories.city_id ORDER BY cities.id");

        return view('data.updateData', ['data' => $data, 'success' => true]);
    }
}
