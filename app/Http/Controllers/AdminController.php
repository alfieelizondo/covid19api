<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UpdatePasswordRequest;
use Auth;
use Hash;
use App\User;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    private function isAdmin()
    {
        return Auth::user()->role == 1;
    }

    public function showCreateUserForm()
    {
        if(!$this->isAdmin())
            return abort(403);

        return view('admin.createUser');
    }

    public function createUser(CreateUserRequest $request)
    {
        if(!$this->isAdmin())
            return abort(403);
        
        $validated = $request->validated();

        $user = new User();
        $user->name = $validated['name'];
        $user->last_name = $validated['last_name'];
        $user->email = $validated['email'];
        $user->password = Hash::make($validated['password']);
        $user->role = $validated['role'];
        $user->save();

        return view('admin.success', ['title' => 'Crear Usuario', 'ret' => 'userList', 'content' => 'El usuario con correo '.$validated['email'].' ha sido creado exitosamente.']);
    }

    public function showUserList()
    {
        if(!$this->isAdmin())
            return abort(403);
        
        return view('admin.userList');
    }

    public function showUpdateUserForm($id)
    {
        if(!$this->isAdmin())
            return abort(403);

        if(User::where('id', $id)->count() == 0)
            return abort(403);

        $user = User::where('id', $id)->first();

        if($user->role < 1)
            return abort(403);

        return view('admin.updateUser', ['id' => $id, 'name' => $user->name, 'last_name' => $user->last_name, 'email' => $user->email, 'role' => $user->role]);
    }

    public function updateUser(UpdateUserRequest $request)
    {
        if(!$this->isAdmin())
            return abort(403);
        
        $validated = $request->validated();
        
        if(User::where('id', $validated['id'])->count() == 0)
            return abort(403);

        $user = User::where('id', $validated['id'])->first();
        $user->name = $validated['name'];
        $user->last_name = $validated['last_name'];
        $user->role = $validated['role'];
        $user->save();

        return view('admin.success', ['title' => 'Actualizar Usuario '.$user->email, 'ret' => 'userList', 'content' => 'El usuario con correo '.$user->email.' ha sido actualizado exitosamente.']);
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        if(!$this->isAdmin())
            return abort(403);
        
        $validated = $request->validated();
        
        if(User::where('id', $validated['id'])->count() == 0)
            return abort(403);

        $user = User::where('id', $validated['id'])->first();
        $user->password = Hash::make($validated['password']);
        $user->save();

        return view('admin.success', ['title' => 'Actualizar Usuario '.$user->email, 'ret' => 'userList', 'content' => 'La contraseña del usuario con correo '.$user->email.' ha sido actualizada exitosamente.']);
    }
}
