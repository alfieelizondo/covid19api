<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\City;

class UpdateDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id' => 'required|integer',
        ];

        $max = City::max('id');

        for($i = 1; $i <= $max; $i++)
        {
            $rules['infected'.$i] = 'integer';
            $rules['healed'.$i] = 'integer';
            $rules['diseased'.$i] = 'integer';
        }

        return $rules;
    }

    public function messages()
    {
        $messages = null;

        $max = City::max('id');

        for($i = 1; $i <= $max; $i++)
        {
            $messages['infected'.$i.'.integer'] = 'El campo infectados debe de ser un número entero.';
            $messages['healed'.$i.'.integer'] = 'El campo curados debe de ser un número entero.';
            $messages['diseased'.$i.'.integer'] = 'El campo muertos debe de ser un número entero.';
        }

        return $messages;
    }
}
