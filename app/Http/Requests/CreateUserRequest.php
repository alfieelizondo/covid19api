<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'c_password' => 'required|same:password',
            'role' => 'required|integer|min:1|max:2',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El campo nombre es obligatorio.',
            'lastName.required' => 'El campo apellido es obligatorio.',
            'email.required' => 'El campo de correo electrónico es obligatorio.',
            'email.email' => 'Ingrese un correo electrónico valido.',
            'email.unique' => 'Esta dirección de correo electrónico ya ha sido registrada.',
            'password.required' => 'El campo contraseña es obligatorio.',
            'c_password.required' => 'El campo confirmar contraseña es obligatorio.',
            'c_password.same' => 'No coincide la contraseña.',
        ];
    }

}
