<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Hash;

class createAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create admin user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $user = new \App\User();
        $user->name = $this->ask('Name?');
        $user->last_name = $this->ask('Last Name?');
        $user->email = $this->ask('Email?');
        $user->password = Hash::make($this->secret('Password?'));
        $user->role = 1;
        $user->save();
        $this->info('Account created ');
    }
}
