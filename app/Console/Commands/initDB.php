<?php

namespace App\Console\Commands;

use App\User;
use App\City;
use App\CityHistory;
use Illuminate\Console\Command;

class initDB extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init:DB';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cities = array(
            'Aguascalientes',
            'Baja California',
            'Baja California Sur',
            'Campeche',
            'Coahuila',
            'Colima',
            'Chiapas',
            'Chihuahua',
            'Ciudad de México',
            'Durango',
            'Guanajuato',
            'Guerrero',
            'Hidalgo',
            'Jalisco',
            'México',
            'Michoacán',
            'Morelos',
            'Nayarit',
            'Nuevo León',
            'Oaxaca',
            'Puebla',
            'Querétaro',
            'Quintana Roo',
            'San Luis Potosí',
            'Sinaloa',
            'Sonora',
            'Tabasco',
            'Tamaulipas',
            'Tlaxcala',
            'Veracruz',
            'Yucatán',
            'Zacatecas'
        );
    
        $codes = array(
            'AG',
            'BC',
            'BS',
            'CM',
            'CO',
            'CL',
            'CS',
            'CH',
            'CX',
            'DG',
            'GT',
            'GR',
            'HG',
            'JC',
            'EM',
            'MI',
            'MO',
            'NA',
            'NL',
            'OA',
            'PU',
            'QT',
            'QR',
            'SL',
            'SI',
            'SO',
            'TB',
            'TM',
            'TL',
            'VE',
            'YU',
            'ZA'
        );

        if(User::all()->count() > 0)
        {
            echo 'Already exists users in the DB.';
            return;
        }

        $user = new User();
        $user->name = 'System';
        $user->last_name = '';
        $user->role = 0;
        $user->email = 'nomail';
        $user->password = '';
        $user->save();

        if(City::all()->count() > 0)
        {
            echo 'Already exists items in the DB.';
            return;
        }


        for($i = 0; $i < 32; $i++)
        {
            $city = new City();
            $city->id = $i+1;
            $city->name = $cities[$i];
            $city->code = $codes[$i];
            $city->save();

            $hist = new CityHistory();
            $hist->infected = 0;
            $hist->healed = 0;
            $hist->diseased = 0;

            $hist->city()->associate($city);
            $hist->user()->associate($user);
            $hist->save();
        }

        echo "Success!\n";
    }
}
