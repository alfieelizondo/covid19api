@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Panel de control</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(Auth::user()->role == 1)
                    <h4>Acciones de administrador:</h4>
                    <p><a class="btn btn-dark" role="button" href="{{ route('createUser') }}">Crear Usuario</a></p>
                    <p><a class="btn btn-dark" role="button" href="{{ route('userList') }}">Listado de usuarios</a></p>
                    @endif
                    <h4>Acciones de usuario:</h4>
                    <p><a class="btn btn-dark" role="button" href="{{ route('updateData') }}">Actualizar datos</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
