@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-light btn-outline-dark btn-lg" href="{{ route('home') }}"><</a>
                    Actualizar datos
                </div>
                <div class="card-body">
                    @if($success)
                    <div class="row text-success">¡Datos actualizados satisfactoriamente!</div>
                    @endif
                    @if($errors->has(null))
                    @foreach ($errors->all() as $error)
                    <div class="row text-danger">{{ $error }}</div>
                    @endforeach
                    @endif
                    <div class="row py-2 bg-dark font-weight-bold text-white">
                        <div class="col-3">Estado</div>
                        <div class="col-2">Infectados</div>
                        <div class="col-2">Curados</div>
                        <div class="col-2">Muertos</div>
                        <div class="col-3"></div>
                    </div>
                    @foreach($data as $item)
                    {!! Form::open(['route' => 'updateData']) !!}
                    <div class="row py-2 @if($item->id % 2 == 0) bg-light @endif">
                        {!! Form::hidden('id', $item->id) !!}
                        <div class="col-3">{{$item->name}}</div>
                        <div class="col-2">{!! Form::text('infected'.$item->id, $item->infected, ['class' => 'form-control input-sm']) !!}</div>
                        <div class="col-2">{!! Form::text('healed'.$item->id, $item->healed, ['class' => 'form-control input-sm']) !!}</div>
                        <div class="col-2">{!! Form::text('diseased'.$item->id, $item->diseased, ['class' => 'form-control input-sm']) !!}</div>
                        <div class="col-3">{!! Form::submit('Actualizar', ['class' => 'btn btn-dark']) !!}</div>
                    </div>
                    {!! Form::close() !!}
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
