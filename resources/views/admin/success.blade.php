@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-light btn-outline-dark btn-lg" href="{{ route($ret) }}"><</a>
                    {{ $title }}
                </div>

                <div class="card-body">
                    <p>
                        {{ $content }}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
