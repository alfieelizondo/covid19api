@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-light btn-outline-dark btn-lg" href="{{ route('home') }}"><</a>
                    Crear Usuario
                </div>

                <div class="card-body">
                
                    {!! Form::open(['route' => 'createUser']) !!}
                        <div class="row">
                            <div class="col text-right">
                                {!! Form::label('name', 'Nombre') !!}
                            </div>
                            <div class="col">
                                {!! Form::text('name', null, ['class' => 'form-control input-sm']) !!}
                            </div>
                            <div class="col">
                                @error('name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col text-right">
                                {!! Form::label('last_name', 'Apellído') !!}
                            </div>
                            <div class="col">
                                {!! Form::text('last_name', null, ['class' => 'form-control input-sm']) !!}
                            </div>
                            <div class="col">
                                @error('last_name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col text-right">
                                {!! Form::label('email', 'Correo') !!}
                            </div>
                            <div class="col">
                                {!! Form::text('email', null, ['class' => 'form-control input-sm']) !!}
                            </div>
                            <div class="col">
                                @error('email')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col text-right">
                                {!! Form::label('password', 'Contraseña') !!}
                            </div>
                            <div class="col">
                                {!! Form::password('password', ['class' => 'form-control input-sm']) !!}
                            </div>
                            <div class="col">
                                @error('password')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="row my-1">
                            <div class="col text-right">
                                {!! Form::label('c_password', 'Confirmar contraseña') !!}
                            </div>
                            <div class="col">
                                {!! Form::password('c_password', ['class' => 'form-control input-sm']) !!}
                            </div>
                            <div class="col">
                                @error('c_password')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="row my-1">
                            <div class="col text-right">
                                {!! Form::label('role', 'Tipo') !!}
                            </div>
                            <div class="col">
                                {!! Form::select('role', ['2' => 'Usuario', '1' => 'Administrador'], null, ['class' => 'form-control input-sm']) !!}
                            </div>
                            <div class="col">
    
                            </div>
                        </div>
                        <div class="row">
                            <div class="col"></div>
                            <div class="col">
                                {!! Form::submit('Crear', ['class' => 'btn btn-dark']) !!}
                            </div>
                            <div class="col"></div>
                        </div>
                    {!! Form::close() !!}

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
